﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCMovement : MonoBehaviour
{
    private NavMeshAgent movement;

    private Animator anim;

    public Transform moveTo;
    // Start is called before the first frame update
    void Start()
    {
        movement = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (moveTo != null)
        {
            movement.SetDestination(moveTo.position);
            anim.SetBool("walk", true);
        }
    }
}
