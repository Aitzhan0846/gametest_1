﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorPlane
{
    Red,
    Blue,
    Yellow
}
public class TirggerCheck : MonoBehaviour
{
    public ColorPlane colorPlane;
    private TaskCheck taskCheck;
    private SpriteFlash flash;

    private GameObject currentNPC;
    private void Start()
    {
        taskCheck = FindObjectOfType<TaskCheck>();
        flash = GetComponent<SpriteFlash>();
    }
    private void OnTriggerEnter(Collider other)
    {
        flash.flashDuration = 0.25f;
        if (other.tag == colorPlane.ToString())
        {
            other.transform.GetComponent<NPCMovement>().enabled = false;
            taskCheck.task_1 += 1;
            flash.flashColor = Color.white;
            taskCheck.Winner();
        }
        else
        {
            flash.flashColor = Color.red;
        }
        flash.InitialColor();
        currentNPC = other.gameObject;
        Invoke("NPCAnimDisable", 1);
    }
    void NPCAnimDisable()
    {
        currentNPC.GetComponent<Animator>().SetBool("walk", false);
    }
    private void OnTriggerExit(Collider other)
    {
        flash.flashDuration = 0;
        if (other.tag == colorPlane.ToString())
        {
            other.transform.GetComponent<NPCMovement>().enabled = true;
            taskCheck.task_1 -= 1;
            taskCheck.Winner();
        }
        flash.InitialColor();

    }
}
