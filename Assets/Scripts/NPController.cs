﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPController : MonoBehaviour
{
    public static List<GameObject> npc=new List<GameObject>();

    private Transform currentNPC;

   
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            Ray MyRay;
            MyRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(MyRay.origin, MyRay.direction * 10, Color.yellow);
            if (Physics.Raycast(MyRay, out hit, 1000))
            {
                print(hit.transform.name + "nameeee0");
                for (int i = 0; i < npc.Count; i++)
                {
                    if (hit.transform.name == npc[i].name)
                    {
                        currentNPC = hit.transform;
                        hit.transform.GetComponent<Outline>().enabled = true;
                        break;
                    }
                    else
                    {
                        
                        if (currentNPC != null)
                        {
                            SelectNPC(hit.transform);
                            //Outline npc disable
                            currentNPC.GetComponent<Outline>().enabled = false;
                            currentNPC = null;
                            break;
                        }
                        
                    }
                }

            }
        }
    }
    void SelectNPC(Transform _moveTO)
    {
        currentNPC.GetComponent<NPCMovement>().moveTo = _moveTO;
    }
}
