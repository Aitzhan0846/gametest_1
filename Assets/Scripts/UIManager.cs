﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum CurrentLevel
{
    Level_1,
    Level_2
}
public class UIManager : MonoBehaviour
{
    public CurrentLevel curLevel;

    [SerializeField] private Text txtCurLevel;
    [SerializeField] private Text txtNextLevel;

    [SerializeField] private ParticleSystem winEffect;
    // Start is called before the first frame update
    void Start()
    {
        
        InitialLevel();

        //Лучший говнокод за последний год!!! ;)
       // txtNextLevel.text =  SceneUtility.GetScenePathByBuildIndex(SceneManager.GetActiveScene().buildIndex+1).Substring(SceneUtility.GetScenePathByBuildIndex(SceneManager.GetActiveScene().buildIndex + 1).LastIndexOf('/')+1, SceneUtility.GetScenePathByBuildIndex(SceneManager.GetActiveScene().buildIndex + 1).Length- SceneUtility.GetScenePathByBuildIndex(SceneManager.GetActiveScene().buildIndex + 1).LastIndexOf('.')+1);
        
    }
    public void InitialLevel()
    {
        txtCurLevel.text = $"Current Level: {SceneManager.GetActiveScene().buildIndex+1}";
        txtNextLevel.text = $"Next Level: {SceneManager.GetActiveScene().buildIndex + 2}";
    }
    public void EnableParticle()
    {
        winEffect.Play();
        Invoke("NextLevel", 2);
    }
   public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
